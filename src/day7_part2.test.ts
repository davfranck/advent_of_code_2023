import fs from 'fs';
import { expect, test } from "vitest";
import { readFileContent, stringWithLineSeparatorToArray } from "./utils";

interface HandType {
    [key: string]: number;
}
const HandTypes:HandType = {
    FIVE_OF_A_KIND: 7,
    FOUR_OF_A_KIND: 6,
    FULL_HOUSE: 5,
    THREE_OF_A_KIND: 4,
    TWO_PAIR: 3,
    ONE_PAIR: 2,
    HIGH_CARD: 1
}
const CARD_ORDER = ['J','2','3','4','5','6','7','8','9','T','Q','K','A']

class Hand {
    type: number;
    value: string;
    bid: number;
    nbByCard: Map<string, number>;
    constructor(value: string, bid: number = 0) {
        this.nbByCard = new Map();
        this.value = value;
        this.type = this.findType(value);
        this.bid = bid;
    }

    findType(value: string): number {
        for(const char of value) {
            this.nbByCard.set(char, (this.nbByCard.get(char) || 0) + 1);
        }
        this.replaceJokers();
        if(this.nbByCard.size === 1) {
            return HandTypes.FIVE_OF_A_KIND;
        } 
        if(this.nbByCard.size === 2) {
            return Array.from(this.nbByCard.values()).some(nb => nb === 4) ? 
                HandTypes.FOUR_OF_A_KIND : HandTypes.FULL_HOUSE;
        }
        if(this.nbByCard.size === 3) {
            return Array.from(this.nbByCard.values()).some(nb => nb === 3) ? 
                HandTypes.THREE_OF_A_KIND : HandTypes.TWO_PAIR;
        }
        if(this.nbByCard.size === 4) {
            return HandTypes.ONE_PAIR;
        }
        return HandTypes.HIGH_CARD;
    }
    replaceJokers(): void {
        if(this.nbByCard.has('J')) { 
            const nbJokers = this.nbByCard.get('J')!;
            this.nbByCard.delete('J')
            if(nbJokers === 5) {
                this.nbByCard.set('A', 5);
                this.nbByCard.delete('J');
                return;
            }
            let maxEntry:[string, number] = ['NO_ENTRY', 0];
            Array.from(this.nbByCard.entries()).forEach(entry => {
                if (entry[1] > maxEntry[1]) {
                    maxEntry = entry;
                } else if(entry[1] === maxEntry[1]) {
                    const indexOfCurrent = CARD_ORDER.indexOf(entry[0])
                    const indexOfMaxEntry = CARD_ORDER.indexOf(maxEntry[0])
                    if(indexOfCurrent > indexOfMaxEntry) {
                        maxEntry = entry;
                    }
                }
            })
            this.nbByCard.set(maxEntry[0], maxEntry[1] + nbJokers);
        }
    }
}
class Hands {
    constructor(public hands: Hand[]){}
    order(): Hands {
        return new Hands(this.hands.sort((a, b) => { 
            const diff = a.type - b.type;
            if (diff === 0) {
                for(let i = 0; i < a.value.length; i++) {
                    const indexOfCardA = CARD_ORDER.indexOf(a.value[i])
                    const indexOfCardB = CARD_ORDER.indexOf(b.value[i])
                    if(indexOfCardA === indexOfCardB) {
                        continue;
                    } 
                    return indexOfCardA - indexOfCardB;
                }
            }
            return diff;
        }))
    }
    totalWinnings(): any {
        return this.order().hands.reduce((acc, hand, index) => acc + (hand.bid * (index + 1)), 0)
    }
}

test.each([
    [[new Hand('J4729', 0), new Hand('J25T9', 0)], [new Hand('J25T9', 0), new Hand('J4729', 0), ]]
])('given hands %o then ordered hands are %o', (hands: Hand[], expectedHands: Hand[]) => {
    expect(new Hands(hands).order().hands).toEqual(expectedHands);
})

test('given sample input then return result', () => {
    const content = readFileContent('day7.txt')
    const lines = stringWithLineSeparatorToArray(content);
    const hands = lines.map(line => {
        const [handValue, handBidAsString] = line.split(" ")
        return new Hand(handValue, Number.parseInt(handBidAsString));
    })

    //fs.writeFile('C:/trash/day7.txt', JSON.stringify(new Hands(hands).order()), { flag: 'a+' }, err => console.log(err));
    expect(new Hands(hands).totalWinnings()).toBe(252127335);
})

test('given sample input then return 6440', () => {
    const content = readFileContent('sample_day7.txt')
    const lines = stringWithLineSeparatorToArray(content);
    const hands = lines.map(line => {
        const [handValue, handBidAsString] = line.split(" ")
        return new Hand(handValue, Number.parseInt(handBidAsString));
    })
    expect(new Hands(hands).totalWinnings()).toBe(5905);
})

test.each([
    [new Hand('AAAAA'), HandTypes.FIVE_OF_A_KIND],
    [new Hand('AAAJA'), HandTypes.FIVE_OF_A_KIND],// Joker
    [new Hand('AAKJK'), HandTypes.FULL_HOUSE],// Joker and two letters => AAKAK
    [new Hand('KKAJA'), HandTypes.FULL_HOUSE],// Joker and two letters => KKAJA
    [new Hand('AAAAK'), HandTypes.FOUR_OF_A_KIND],
    [new Hand('AKAAK'), HandTypes.FULL_HOUSE],
    [new Hand('A8AAK'), HandTypes.THREE_OF_A_KIND],
    [new Hand('A8A78'), HandTypes.TWO_PAIR],
    [new Hand('A8678'), HandTypes.ONE_PAIR],
    [new Hand('A2678'), HandTypes.HIGH_CARD],
    [new Hand('JJ29T'), HandTypes.THREE_OF_A_KIND],
])('given a hand %o when type then %i', (hand: Hand, expectedHandType: number) => {
    expect(hand.type).toEqual(expectedHandType);
})