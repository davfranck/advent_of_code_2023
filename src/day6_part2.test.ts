import { expect, test } from "vitest";
import { readFileContent, stringWithLineSeparatorToArray } from "./utils";

class Race{
    
    constructor(public time: number, public bestDistance: number) {}

    static from(lines: string[]): Race {
        const time = parseInt(lines[0].replace(/\D/g, ''));
        const distance = parseInt(lines[1].replace(/\D/g, ''));
        return new Race(time, distance);
    }

    bestHoldMilliseconds(): number[] {
        return Array.from({ length: this.time - 1 }, (_, i) => i + 1)
                .filter(i => this.millimetersForTime(i) > this.bestDistance);
    }

    millimetersForTime(holdTime: number) {
        return (this.time - holdTime) * holdTime;
    }
}

test('given the part 1 input file then return the result', () => {
    const content = readFileContent('day6.txt')
    const lines = stringWithLineSeparatorToArray(content)
    const result = Race.from(lines).bestHoldMilliseconds().length
    expect(result).toBe(23654842)
})

test('given the part 1 sample input file then return the result', () => {
    const content = readFileContent('sample_day6_part1.txt')
    const lines = stringWithLineSeparatorToArray(content)
    const result = Race.from(lines).bestHoldMilliseconds().length
    expect(result).toBe(71503)
})

test('given the sample input file then return race', () => {
    const content = readFileContent('sample_day6_part1.txt')
    const lines = stringWithLineSeparatorToArray(content)
    expect(Race.from(lines)).toEqual(new Race(71530, 940200))
})

test. each([
    [new Race(7, 19), []],
    [new Race(7, 9), [2,3,4,5]],
    [new Race(15, 40), [4,5,6,7,8,9,10,11]],
    [new Race(30, 200), [11,12,13,14,15,16,17,18,19]],
])('given a race %o when betterHoldMilliseconds then return the milliseconds %o that improve the record', 
        (race:Race, expectedBetterHoldMilliseconds: number[]) => {
    expect(race.bestHoldMilliseconds()).toEqual(expectedBetterHoldMilliseconds);
})

test.each([
    [1, 7, 6],
    [1, 6, 5],
    [2, 7, 10],
    [3, 7, 12],
    [6, 7, 6],
])('given a hold time %i and a race time %i then return distance %i', (holdTime: number, raceTime: number, distance: number) =>{
    expect(new Race(raceTime, 0).millimetersForTime(holdTime)).toBe(distance)
})