import { expect, test } from "vitest"
import { readFileContent, stringWithLineSeparatorToArray } from "./utils";

interface Numbers {
    [numberText: string]: number;
}
  
const NUMBERS: Numbers = {
    "one": 1,
    "two": 2,
    "three": 3,
    "four": 4,
    "five": 5,
    "six": 6,
    "seven": 7,
    "eight": 8,
    "nine": 9,
};

function findFirst(word: string): number {
    for (let i = 0; i < word.length; i++) {
        const result = findMatchingNumber(word, i)
        if(result != null) {
            return result
        }
    }
    throw new Error('No number found')
}  

function findLast(word: string): number  {
    for (let i = word.length - 1; i >= 0; i--) {
        const result = findMatchingNumber(word, i)
        if(result != null) {
            return result
        }
    }
    throw new Error('No number found')
}

const findMatchingNumber = (word: string, i: number): number | null => {
    const currentChar = word[i];
    if(Number.isInteger(+currentChar)) {
        return parseInt(currentChar);
    } else {
        const stringFromIndexI = word.substring(i)
        for (const numberText in NUMBERS) {
            if (stringFromIndexI.startsWith(numberText)) {
                return NUMBERS[numberText];
            }
        }
    }
    return null;
}

function calibration(word: string): number {
    const firstDigit = findFirst(word);
    const lastDigit = findLast(word);

    return parseInt(firstDigit.toString() + lastDigit.toString());
}

const calibrationTotal = (values: string[]): number => {
    return values.reduce((sum, value) => sum + calibration(value), 0);
}

function calibrationTotalForFile(filename: string): number {
    const fileContent = readFileContent(filename);
    const linesArray = stringWithLineSeparatorToArray(fileContent);
    return calibrationTotal(linesArray);
}

test('replace in the order of the word only', () => {
    expect(calibration('pcg91vqrfpxxzzzoneightzt')).toBe(98)
})

test('calibration value for an input with only numbers', () => { 
    expect(calibration('12')).toBe(12)
})

test('calibration value for an input with only one number', () => { 
    expect(calibration('7v')).toBe(77)
})

test('calibration value for an input with one number at the beginning and one at the end', () => { 
    expect(calibration('1ab2')).toBe(12)
})

test('calibration value for an input with another number betwwen', () => { 
    expect(calibration('fgdgf1abfd5sf2ds')).toBe(12)
})

test('calibration value for an input with only one digit', () => { 
    expect(calibration('treb7uchet')).toBe(77)
})

test('given a string with digit as text they should be transform as digit before calibration', () => {
    expect(calibration('eightwo25azaseven')).toBe(87)
})

test('calibration sum for all values to calibrate', () => { 
    expect(calibrationTotal(['1abc2', 'pqr3stu8vwx', 'a1b2c3d4e5f', 'treb7uchet'])).toBe(142)
})

test('given a sample file in the src/input dir then compute the calibration sum', () => {
    const filename = 'sample_day1_part1.txt'
    expect(calibrationTotalForFile(filename)).toBe(142)
});

test('given sample file for part 2in the src/input dir then compute the calibration sum', () => {
    const filename = 'sample_day1_part2.txt'
    expect(calibrationTotalForFile(filename)).toBe(281)
});

test('given the input file in the src/input dir then compute the calibration sum', () => {
    const filename = 'day1_part1.txt'
    expect(calibrationTotalForFile(filename)).toBe(55902)
});