import { expect, test } from "vitest"
import { readFileContent, stringWithLineSeparatorToArray } from "./utils";

class Game {
    randomDraws: CubesDraw[];
    id: number;
    constructor(randomDraws: CubesDraw[], id: number) {
        this.randomDraws = randomDraws;
        this.id = id;
    }
    isValid(): boolean {
        return this.randomDraws.every(
            randomDraw => 
                randomDraw.blue <= 14 && 
                randomDraw.green <= 13 && 
                randomDraw.red <= 12)
    }

    minCubes(): CubesDraw {
        let red = 0;
        let green = 0;
        let blue = 0;
        this.randomDraws.forEach(draw => {
            blue = draw.blue > blue ? draw.blue : blue;
            red = draw.red > red ? draw.red : red;
            green = draw.green > green ? draw.green : green;
        })
        return new CubesDraw(blue, red, green)
    }
}

class CubesDraw {
    blue: number;
    red: number;
    green: number;

    constructor(blue:number, red: number, green: number) {
        this.blue = blue;
        this.red = red;
        this.green = green;
    }

    power(): number {
        return this.blue * this.red * this.green;
    }

    static fromString(randomDrawsAsString: string): CubesDraw {
        const randomDraws = randomDrawsAsString.split(", ")
        const colorDraws = randomDraws.map(randomDraw => OneCubeDraw.fromString(randomDraw.trim())) 
        return new CubesDraw(
            CubesDraw.getColorValue(colorDraws, "blue"), 
            CubesDraw.getColorValue(colorDraws, "red"), 
            CubesDraw.getColorValue(colorDraws, "green"))
    }
    static getColorValue(colorDraws: OneCubeDraw[], searchedColor: string): number {
        const value = colorDraws.find(cd => cd.color === searchedColor)?.value
        return value != undefined ? value : 0;
    }
}

class OneCubeDraw {
    color: string;
    value: number;
    constructor(color: string, value: number) {
        this.color = color;
        this.value = value;
    }
    static fromString(colorDrawAsString: string): OneCubeDraw {
        const [numberStr, color] = colorDrawAsString.trim().split(" ");
        const value = parseInt(numberStr);
        return new OneCubeDraw(color, value)
    }
}

function mapGame(gameAsString: string): Game {
    const [gameAndId, randomDrawsAsString] = gameAsString.split(":");
    const series = randomDrawsAsString.trim().split(";")
    const randomDraws = series.map(serie => CubesDraw.fromString(serie));
    const [game, idAsString] = gameAndId.split(" ")
    return new Game(randomDraws, parseInt(idAsString));
}

// PART 1
function sumOfValidGames(input: string): any {
    const lines = readFileContent(input);
    const linesAsArrayOfString = stringWithLineSeparatorToArray(lines);
    const games = linesAsArrayOfString.map(line => mapGame(line));
    return games.filter(game => game.isValid())
                .map(game => game.id)
                .reduce((sum, current) => sum + current, 0)
}

// PART 2
function sumOfPower(input: string): any {
    const lines = readFileContent(input);
    const linesAsArrayOfString = stringWithLineSeparatorToArray(lines);
    const games = linesAsArrayOfString.map(line => mapGame(line));
    return games
        .map(game => game.minCubes())
        .map(randomDraw => randomDraw.power())
        .reduce((acc, cur) => acc + cur, 0);
}

test('sum of power for all', () => {
    expect(sumOfPower('day2.txt')).toBe(66016);
})

test('sum of power for sample', () => {
    expect(sumOfPower('sample_day2_part1.txt')).toBe(2286);
})

test('power of a random draw', () => {
    expect(new CubesDraw(6, 4, 2).power()).toBe(48)
})

test('given a game when minCubes then return the minimum of cubes needed', () => {
    expect(new Game([new CubesDraw(1,1,1)], 1).minCubes()).toEqual(new CubesDraw(1,1,1))
    const gameWith2Draw = new Game([new CubesDraw(1,4,6), new CubesDraw(2,3,4)], 1)
    expect(gameWith2Draw.minCubes()).toEqual(new CubesDraw(2,4,6))
})

test('given the input of day 1 then return the sum of the valid games id', () => {
    expect(sumOfValidGames('day2.txt')).toBe(2541);
})

test('given a list of sample games then return the sum of the valid games id', () => {
    expect(sumOfValidGames('sample_day2_part1.txt')).toBe(8);
})

test('given a valid game with one random draw when test if valid then return true', () => {
    expect(new Game([new CubesDraw(1,1,1)], 1).isValid()).toBe(true);
    expect(new Game([new CubesDraw(14,12,13)], 1).isValid()).toBe(true);
    expect(new Game([new CubesDraw(14,2,2), new CubesDraw(1,12,13)], 1).isValid()).toBe(true);
})

test('given an invalid game with one random draw when test if valid then return false', () => {
    expect(new Game([new CubesDraw(15,1,1)], 1).isValid()).toBe(false);
    expect(new Game([new CubesDraw(1,13,1)], 1).isValid()).toBe(false);
    expect(new Game([new CubesDraw(1,1,1), new CubesDraw(1,1,14)], 1).isValid()).toBe(false);
    expect(new Game([new CubesDraw(1,3,1), new CubesDraw(15,1,1)], 1).isValid()).toBe(false);
})

test('when many random draws then return Game', () => {
    expect(mapGame('Game 101: 11 blue, 22 red, 33 green;4 red, 1 green, 2 blue')).toEqual(new Game([
        new CubesDraw(11,22,33),
        new CubesDraw(2,4,1)],
        101
    ))
})

test('when invalid game as string then isValid is false', () => {
    expect(mapGame('Game 99: 11 green, 4 red, 12 blue; 9 red, 4 blue; 20 green, 6 blue')).toEqual(new Game(
        [new CubesDraw(12,4,11), new CubesDraw(4,9,0), new CubesDraw(6,0,20)],
        99
    ))
})

test('when invalid game as string then isValid is false', () => {
    expect(mapGame('Game 99: 11 green, 4 red, 12 blue; 9 red, 4 blue; 20 green, 6 blue').isValid()).toEqual(false)
})

test('given a color draw as string then map to a ColorDraw', () => {
    expect(OneCubeDraw.fromString('23 blue')).toEqual(new OneCubeDraw("blue", 23))
    expect(OneCubeDraw.fromString('46 red')).toEqual(new OneCubeDraw("red", 46))
    expect(OneCubeDraw.fromString('86 green')).toEqual(new OneCubeDraw("green", 86))
})

test('given a serie "6 blue" then map to object', () => {
    expect(CubesDraw.fromString('3 blue')).toEqual(new CubesDraw(3, 0, 0))
})

test('given a serie "21 blue, 33 red" then map to object', () => {
    expect(CubesDraw.fromString('21 blue, 33 red')).toEqual(new CubesDraw(21, 33, 0))
})

test('given a random draw "2 red, 48 green, 20 blue" then map to object', () => {
    expect(CubesDraw.fromString('2 red, 48 green, 20 blue')).toEqual(new CubesDraw(20, 2, 48))
})