import { expect, test } from "vitest";
import { readFileContent, stringWithLineSeparatorToArray } from "./utils";

class Card {
    copies: number;
    winningNumbers: number[];
    scratchedNumbers: number[];
    id: number;

    constructor(winningNumbers: number[], scratchedNumbers: number[], id: number) {
        this.winningNumbers = winningNumbers;
        this.scratchedNumbers = scratchedNumbers;
        this.id = id;
        this.copies = 0;
    }

    static incrementCopies(cards: Card[], ids: number[], cardCopies: number) {
        cards.filter(card => ids.includes(card.id)).
                forEach(card => card.incrementCopy(cardCopies + 1))
    }

    static increment(cards: Card[]) {
        cards.forEach(card => Card.incrementCopies(cards, card.idsToDuplicate(), card.copies))
    }

    incrementCopy(times: number = 1): void {
       this.copies = this.copies + (1 * times);
    }

    scracthedWinners(): number [] {
        return this.winningNumbers.filter(nb => this.scratchedNumbers.includes(nb))
    }

    points() : number {
        const nbScratchedWinners = this.scracthedWinners().length;
        return nbScratchedWinners === 0 ? 0 : Card.by2Times(1, nbScratchedWinners - 1);
    }

    idsToDuplicate(): number[] {
        return Array.from({ length: this.scracthedWinners().length }, (_, i) => (this.id + 1) + i);
    }

    static by2Times(current: number, times: number) : number {
        return times === 0 ? current : this.by2Times(current * 2, times - 1);
    }

    static from (input:string) {
        const [cardAndIndex, numbers] = input.split(':');
        const [winningStr, scracthedStr] = numbers.split("|")
        return new Card(
            winningStr.match(/\d+/g)!.map(nb => Number.parseInt(nb)),
            scracthedStr.match(/\d+/g)!.map(nb => Number.parseInt(nb)),
            cardAndIndex.match(/\d+/)!.map(nbAsStr => Number.parseInt(nbAsStr))[0]
        )
    }
}

test('given a card then map to card', () => {
    expect(Card.from('Card 16: 41 48 83 86 17 | 83 86  6 31 17  9 48 53')).toEqual(
        new Card([41, 48, 83, 86, 17], [83, 86, 6, 31, 17, 9, 48, 53], 16)
    )
})

test.each([
    [new Card([1, 2, 3], [2, 3, 4], 1), [2, 3]],
    [new Card([1, 2, 3], [6, 8, 49], 1), []],
])('given a card then return winning numbers', (card:Card, scracthedWinners: number[]) => {
    expect(card.scracthedWinners()).toEqual(scracthedWinners)
})

test.each([
    [new Card([1, 2, 3], [7, 50, 4], 1), 0],
    [new Card([1, 2, 3], [2, 5, 4], 2), 1],
    [new Card([1, 2, 3], [2, 3, 4], 3), 2],
    [new Card([41, 48, 83, 86, 17], [83, 86, 6, 31, 17, 9, 48, 53], 4), 8],
])('given a card %o then return the points %i', (card: Card, expectedPoints: number) => {
    expect(card.points()).toBe(expectedPoints)
})

test('given sample part 1 input the return 13 points', () => {
    const input = readFileContent('sample_day4_part1.txt');
    const cards = stringWithLineSeparatorToArray(input).map(gameAsStr => Card.from(gameAsStr));
    const sum = cards.reduce((acc, card) => acc + card.points(), 0)
    expect(sum).toBe(13);
})

test('given part 1 input the return points', () => {
    const input = readFileContent('day4.txt');
    const cards = stringWithLineSeparatorToArray(input).map(gameAsStr => Card.from(gameAsStr));
    const sum = cards.reduce((acc, card) => acc + card.points(), 0)
    expect(sum).toBe(26914);
})

test.each([
    [new Card([1, 2], [2, 3], 1), [2]], // un winner
    [new Card([1, 2, 3], [2, 3], 1), [2, 3]], // deux winners
])('given a card %o when list id to duplicates then return ids %o to increment', (card: Card, ids: number[]) => {
    expect(card.idsToDuplicate()).toEqual(ids)
})

test.each([
    [[new Card([], [], 1), new Card([], [], 2), new Card([], [], 3)], [1, 2]],
    [[new Card([], [], 1), new Card([], [], 2), new Card([], [], 3)], [1, 2, 5, 6]],
])('given a list of cards %o when increment copies from list %o then copies is updated', (cards:Card[], ids: number[]) => {
    Card.incrementCopies(cards, ids, 0)
    expect(cards.find(card => card.id === 1)?.copies).toBe(1)
    expect(cards.find(card => card.id === 2)?.copies).toBe(1)
    expect(cards.find(card => card.id === 3)?.copies).toBe(0)
}) 

test('given a list of cards when process copies then return sum', () => {
    const input = readFileContent('sample_day4_part1.txt');
    const cards = stringWithLineSeparatorToArray(input).map(gameAsStr => Card.from(gameAsStr));
    Card.increment(cards)
    expect(cards.reduce((acc, card) => acc + card.copies + 1, 0)).toBe(30)  
})

test('part2', () => {
    const input = readFileContent('day4.txt');
    const cards = stringWithLineSeparatorToArray(input).map(gameAsStr => Card.from(gameAsStr));
    Card.increment(cards)
    expect(cards.reduce((acc, card) => acc + card.copies + 1, 0)).toBe(13080971)  
})

// arrêter le calcul quand il n'y a plus de scratchcard gagnée