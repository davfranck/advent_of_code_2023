import { expect, test } from "vitest";
import { readFileContent, stringWithLineSeparatorToArray } from "./utils";

class Node {
    static regex = /(\w+)\s*=\s*\((\w+),\s*(\w+)\)/;
    constructor(public name: string, public leftNodeName: string, public rightNodeName: string) {}
    static from(line: string): any {
        const [_, name, left, right] = line.match(Node.regex)!;
        return new Node(name, left, right);
    }
}
class Network {
    nodesByName: Map<string, Node>;
    startNode: Node;
    constructor(nodes: Node[]) {
        this.startNode = nodes[0];
        this.nodesByName = new Map();
        nodes.forEach(node => this.nodesByName.set(node.name, node));
    }
    static from(lines: string[]): Network {
        const nodes = lines.map(line => Node.from(line))
        return new Network(nodes)
    }
    nbStepsWith(directions: string): number {
        let currentNode = this.startNode
        let nbSteps = 0;
        let currentDirectionIndex = 0;
        while(!currentNode.name.endsWith('Z')) {
            currentDirectionIndex = directions.length === currentDirectionIndex ? 0 : currentDirectionIndex
            const currentDirection =  directions.charAt(currentDirectionIndex)
            const nextNodeName = currentDirection === 'L' ? currentNode.leftNodeName : currentNode.rightNodeName
            currentNode = this.nodesByName.get(nextNodeName)!
            currentDirectionIndex++;
            nbSteps++;
        }
        return nbSteps;
    }
}

test('given input then return nb of steps', () => {
    const content = readFileContent('day8.txt')
    const lines = stringWithLineSeparatorToArray(content)
    const directions = lines.shift()!
    const network = Network.from(lines)
    expect(network.nbStepsWith(directions)).toBe(13019)
})

test('given sample input then return nb of steps', () => {
    const content = readFileContent('sample_day8.txt')
    const lines = stringWithLineSeparatorToArray(content)
    const directions = lines.shift()!
    const network = Network.from(lines)
    expect(network.nbStepsWith(directions)).toBe(2)
})

test.each([
   [[new Node('AAA', 'ZZZ', 'ZZZ'), new Node('ZZZ', 'ZZZ', 'ZZZ')], 'L', 1],// A->Z
   [[new Node('AAA', 'BBB', 'BBB'), new Node('BBB', 'ZZZ', 'ZZZ'), new Node('ZZZ', 'ZZZ', 'ZZZ')], 'LL', 2],//A->B->Z
   [[new Node('AAA', 'BBB', 'BBB'), new Node('BBB', 'AAA', 'CCC'), new Node('CCC', 'ZZZ', 'ZZZ'), new Node('ZZZ', 'ZZZ', 'ZZZ')], 'LLRRR', 5],//A->B->A->B->C->Z
   [[new Node('AAA', 'BBB', 'XXX'), new Node('BBB', 'AAA', 'CCC'), new Node('CCC', 'ZZZ', 'XXX'), new Node('ZZZ', 'ZZZ', 'ZZZ')], 'LLLR', 5],// A->B->A->B->C->Z
   [[new Node('AAA', 'BBB', 'BBB'), new Node('BBB', 'AAA', 'ZZZ'), new Node('ZZZ', 'ZZZ', 'ZZZ')], 'LLR', 6]
])
        ('given a network and directions when navigate then return nb of steps', (nodes: Node[], directions: string, expectedSteps: number) => {
    expect(new Network(nodes).nbStepsWith(directions)).toBe(expectedSteps)
})