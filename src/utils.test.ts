import { expect, test } from "vitest"
import { readFileContent, readFileContentAsync, stringWithLineSeparatorToArray } from "./utils"

test('read the content of a file in the src/input folder, async function', async () => {
    const expected = `aaa
bbb`
    expect(await readFileContentAsync('sample.txt')).toEqual(expected);
})

test('given a filename when readFileContent then content of the file is returned', () => {
    const expected = `aaa
bbb`
    expect(readFileContent('sample.txt')).toEqual(expected);
})

test('given a string with line separator \n when stringToArray then an array is returned', () => {
    const input = `aa
bb`
    expect(stringWithLineSeparatorToArray(input)).toEqual(['aa', 'bb'])
})