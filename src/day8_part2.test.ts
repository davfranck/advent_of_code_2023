import { expect, test } from "vitest";
import { readFileContent, stringWithLineSeparatorToArray } from "./utils";

class Node {
    static regex = /(\w+)\s*=\s*\((\w+),\s*(\w+)\)/;
    constructor(public name: string, public leftNodeName: string, public rightNodeName: string) {}
    static from(line: string): any {
        const [_, name, left, right] = line.match(Node.regex)!;
        return new Node(name, left, right);
    }
}
class Network {
    nodesByName: Map<string, Node>;
    startNode: Node;
    constructor(nodes: Node[]) {
        this.startNode = nodes[0];
        this.nodesByName = new Map();
        nodes.forEach(node => this.nodesByName.set(node.name, node));
    }
    static from(lines: string[]): Network {
        const nodes = lines.map(line => Node.from(line))
        return new Network(nodes)
    }
    startingNodes(): Node[] {
        return Array.from(this.nodesByName.values()).filter(entry => entry.name.endsWith('A'))
    }
    nbStepsWith(directions: string): any {
        let currentNodes = this.startingNodes()
        let nbSteps = 0;
        let currentDirectionIndex = 0;
        while(!currentNodes.every(node => node.name.endsWith('Z'))) {
            currentDirectionIndex = directions.length === currentDirectionIndex ? 0 : currentDirectionIndex
            const currentDirection =  directions.charAt(currentDirectionIndex)
            currentNodes = currentNodes.map(currentNode => currentDirection === 'L' ? currentNode.leftNodeName : currentNode.rightNodeName)
                                       .map(nodeName => this.nodesByName.get(nodeName)!)
            currentDirectionIndex++;
            nbSteps++;
            //console.log(`current direction : ${currentDirection} nb steps : ${nbSteps} current nodes : `, currentNodes)
            if(nbSteps > 100) break;
            // Je viens ce qu'il fallait faire en fait en regardant sur reddit, du coup j'arrête la partie 2 ici. 
        }
        return nbSteps;
    }
}

const networkSampleAsString = `11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)`;


test.skip('given input then return nb of steps', () => {
    const content = readFileContent('day8.txt')
    const lines = stringWithLineSeparatorToArray(content)
    const directions = lines.shift()!
    const network = Network.from(lines)
    expect(network.nbStepsWith(directions)).toBe(2)
})

test('given network when nagivate until both at Z then return number of steps', () => {
    const lines = stringWithLineSeparatorToArray(networkSampleAsString)
    expect(Network.from(lines).nbStepsWith('LR')).toBe(6)
})

test('given input then return nb of steps', () => {
    const content = readFileContent('day8.txt')
    const lines = stringWithLineSeparatorToArray(content)
    const directions = lines.shift()!
    const network = Network.from(lines)
    const startingNodes = network.startingNodes()
    expect(startingNodes.length).toBe(6)
})

test.each([
    [networkSampleAsString, [new Node('11A', '11B', 'XXX'), new Node('22A', '22B', 'XXX')]],
])
        ('given a network then find starting nodes', (linesAsString: string, expectedStartingNodes: Node[]) => {
    const lines = stringWithLineSeparatorToArray(linesAsString)
    expect(Network.from(lines).startingNodes()).toEqual(expectedStartingNodes)
})