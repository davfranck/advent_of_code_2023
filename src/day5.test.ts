import { expect, test } from "vitest";

class Line {
    constructor(
        public destinationRangeStart: number,
        public sourceRangeStart: number,
        public rangeLength: number) {}
    isInRange(input: number): boolean {
        return input >= this.sourceRangeStart && input < this.sourceRangeStart + this.rangeLength;
    }
}
class Map {
    constructor(public lines:Line[]) {}
    findMatchingLine(input: number): Line {
        return this.lines.find(line => line.isInRange(input))!
    }
}

test.each([
    [[new Line(50, 98, 2), new Line(52, 50, 48)], 79, 1],
    [[new Line(50, 98, 2), new Line(52, 50, 48)], 99, 0],
])('given a map when findMatchingLine the return the line that contains the input value', (lines: Line[], input: number, expectedIndex: number) => {
    expect(new Map(lines).findMatchingLine(input)).toEqual(lines[expectedIndex])
})

test.each([
    [new Line(50, 98, 2), 79, false],
    [new Line(50, 98, 2), 98, true],
    [new Line(50, 98, 2), 99, true],
    [new Line(50, 98, 2), 100, false],
    [new Line(50, 98, 2), 97, false],
])('given a line %o and an input number %i when isInRange then %s', (line: Line, input: number, expected: boolean) => {
    expect(line.isInRange(input)).toBe(expected);
})
