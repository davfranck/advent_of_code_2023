import fs from 'fs';
import path from 'path';

export async function readFileContentAsync(filename: string): Promise<string> {
    const filePath = path.join('src', 'input', filename);
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    });
}
export function readFileContent(filename: string): string {
    const filePath = path.join('src', 'input', filename);
    try {
        const data = fs.readFileSync(filePath, 'utf8');
        return data;
    } catch (err) {
        throw err;
    }
}

export const stringWithLineSeparatorToArray = (input: string): string[] => {
    return input.split('\n').map(line => line.trim()).filter(line => line !== '');
}