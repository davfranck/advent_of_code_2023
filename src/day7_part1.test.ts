import { expect, test } from "vitest";
import { readFileContent, stringWithLineSeparatorToArray } from "./utils";

interface HandType {
    [key: string]: number;
}
const HandTypes:HandType = {
    FIVE_OF_A_KIND: 7,
    FOUR_OF_A_KIND: 6,
    FULL_HOUSE: 5,
    THREE_OF_A_KIND: 4,
    TWO_PAIR: 3,
    ONE_PAIR: 2,
    HIGH_CARD: 1
}

class Hand {
    type: number;
    value: string;
    bid: number;
    constructor(value: string, bid: number = 0) {
        this.value = value;
        this.type = this.findType(value);
        this.bid = bid;
    }

    findType(value: string): number {
        const map = new Map<string, number>()
        for(const char of value) {
            map.set(char, (map.get(char) || 0) + 1);
        }
        if(map.size === 1) {
            return HandTypes.FIVE_OF_A_KIND;
        } 
        if(map.size === 2) {
            return Array.from(map.values()).some(nb => nb === 4) ? 
                HandTypes.FOUR_OF_A_KIND : HandTypes.FULL_HOUSE;
        }
        if(map.size === 3) {
            return Array.from(map.values()).some(nb => nb === 3) ? 
                HandTypes.THREE_OF_A_KIND : HandTypes.TWO_PAIR;
        }
        if(map.size === 4) {
            return HandTypes.ONE_PAIR;
        }
        return HandTypes.HIGH_CARD;
    }
}
const CARD_ORDER = ['2','3','4','5','6','7','8','9','T','J','Q','K','A']
class Hands {
    constructor(public hands: Hand[]){}
    order(): Hands {
        return new Hands(this.hands.sort((a, b) => { 
            const diff = a.type - b.type;
            if (diff === 0) {
                for(let i = 0; i < a.value.length; i++) {
                    const indexOfCardA = CARD_ORDER.indexOf(a.value[i])
                    const indexOfCardB = CARD_ORDER.indexOf(b.value[i])
                    if(indexOfCardA === indexOfCardB) {
                        continue;
                    } 
                    return indexOfCardA - indexOfCardB;
                }
            }
            return diff;
        }))
    }
    totalWinnings(): any {
        return this.order().hands.reduce((acc, hand, index) => acc + (hand.bid * (index + 1)), 0)
    }
}
test('given sample input then return result', () => {
    const content = readFileContent('day7.txt')
    const lines = stringWithLineSeparatorToArray(content);
    const hands = lines.map(line => {
        const [handValue, handBidAsString] = line.split(" ")
        return new Hand(handValue, Number.parseInt(handBidAsString));
    })
    expect(new Hands(hands).totalWinnings()).toBe(250898830);
})

test('given sample input then return 6440', () => {
    const content = readFileContent('sample_day7.txt')
    const lines = stringWithLineSeparatorToArray(content);
    const hands = lines.map(line => {
        const [handValue, handBidAsString] = line.split(" ")
        return new Hand(handValue, Number.parseInt(handBidAsString));
    })
    expect(new Hands(hands).totalWinnings()).toBe(6440);
})

test.each([
    [
        [new Hand('32T3K', 765), new Hand('T55J5', 684), new Hand('KK677', 28), new Hand('KTJJT', 220), new Hand('QQQJA', 483)],
        6440
    ]
])('given a list of hands %o when totalWinnings is %i', (hands: Hand[], expectedTotal: number) => {
    expect(new Hands(hands).totalWinnings()).toBe(expectedTotal);
})

test.each([
    [// order by strength
        [new Hand('AAAA5'), new Hand('AAAAA'), new Hand('AKTA7'), new Hand('JJK2J')],
        [new Hand('AKTA7'), new Hand('JJK2J'), new Hand('AAAA5'), new Hand('AAAAA')],
    ],
    [// order by strength and card order
        [new Hand('AAAAA'), new Hand('KKKKK'), new Hand('AKTA7'), new Hand('TJT67')],
        [new Hand('TJT67'), new Hand('AKTA7'), new Hand('KKKKK'), new Hand('AAAAA')],
    ],
])('given a list of hands %o when order then the list is ordered from the weakest to the strongest : %o', (hands: Hand[], orderedHands: Hand[]) => {
    expect(new Hands(hands).order().hands).toEqual(orderedHands)
})

test.each([
    [new Hand('AAAAA'), HandTypes.FIVE_OF_A_KIND],
    [new Hand('AAAAK'), HandTypes.FOUR_OF_A_KIND],
    [new Hand('AKAAK'), HandTypes.FULL_HOUSE],
    [new Hand('A8AAK'), HandTypes.THREE_OF_A_KIND],
    [new Hand('A8A78'), HandTypes.TWO_PAIR],
    [new Hand('A8678'), HandTypes.ONE_PAIR],
    [new Hand('A2678'), HandTypes.HIGH_CARD],
])('given a hand %o when type then five of a kind', (hand: Hand, expectedHandType: number) => {
    expect(hand.type).toEqual(expectedHandType);
})