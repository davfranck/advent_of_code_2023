import { expect, test } from "vitest"
import { readFileContent, stringWithLineSeparatorToArray } from "./utils"

class GridSymbolWithNumber {
    constructor(public rowIndex: number, public columnIndex: number, public gridNumberValue: number) {}
}

function nextNumberOnRow(row: string, startIndex: number, rowIndex: number): GridNumber {
    let start = -1;
    let numberAsStr = ''
    for(let i = startIndex; i < row.length; i++) {
        const currentValue = Number.parseInt(row[i], 10)
        if (Number.isInteger(currentValue)) {
            if(start === -1) {
                start = i;
            }
            numberAsStr += currentValue;
        } else if(numberAsStr !== '') {
            break;
        }
    }
    if(start == -1) {
        return new NotFoundGridNumber();
    }
    return new GridNumber(start, Number.parseInt(numberAsStr), rowIndex)
}

function sumOfRowPartNumbers(rows: string[], rowIndex: number) : number {
    let currentIndex = 0;
    let partNumber = nextNumberOnRow(rows[rowIndex], currentIndex, rowIndex)
    let sum = 0;
    while (!(partNumber instanceof NotFoundGridNumber)) {
        if(partNumber.isPartNumber(rows)) {
            sum += partNumber.value
        }
        currentIndex = partNumber.endIndex() + 1
        partNumber = nextNumberOnRow(rows[rowIndex], currentIndex, rowIndex)
    }
    return sum;
}

function sumOfPartNumbers(rows: string[]): any {
    let sum = 0;
    for(let rowIndex = 0; rowIndex < rows.length; rowIndex ++) {
        const rowSum = sumOfRowPartNumbers(rows, rowIndex)
        sum += rowSum 
    }
    return sum;
}

class GridNumber {
    constructor(public startIndex: number, public value: number, public rowIndex: number) {}
    
    isPartNumber(rows: string[]): boolean {
        const symbols = this.symbolsAround(rows)
        return symbols.length != 0
    }

    symbolsAround(rows: string[]): GridSymbolWithNumber[] {
        const result = []
        // check à droite même ligne
        const sameRow = rows[this.rowIndex]
        if(this.endIndex() + 1 < sameRow.length && sameRow.charAt(this.endIndex() + 1) !== '.') {
            result.push(new GridSymbolWithNumber(this.rowIndex, this.endIndex() +1, this.value));
        }
        // à gauche même ligne
        if(this.startIndex !== 0 && sameRow.charAt(this.startIndex - 1) !== '.') {
            result.push(new GridSymbolWithNumber(this.rowIndex, this.startIndex - 1, this.value));
        }
        // en dessous
        if(rows.length > this.rowIndex + 1) {
            const rowBelow = rows[this.rowIndex + 1]
            const symbolOnLine = this.symbolOnLine(sameRow.length, rowBelow, this.rowIndex + 1)
            if(symbolOnLine != null) {
                result.push(symbolOnLine);
            }
        }
        // au dessus
        if(this.rowIndex - 1 >= 0) {
            const rowAbove = rows[this.rowIndex - 1]
            const symbolOnLine = this.symbolOnLine(sameRow.length, rowAbove, this.rowIndex - 1)
            if(symbolOnLine != null) {
                result.push(symbolOnLine);
            }
        }

        return result;
    }

    symbolOnLine(rowLength: number, row: string, rowIndex: number): GridSymbolWithNumber | null {
        const leftLimit = this.startIndex == 0 ? 0 : this.startIndex - 1
        const rightLimit = this.endIndex() + 1 === rowLength ? rowLength - 1: this.endIndex() + 1
        for(let i = leftLimit; i <= rightLimit; i++) {
            if(row.charAt(i) !== '.') {
                return new GridSymbolWithNumber(rowIndex, i, this.value);
            }
        }
        return null;
    }

    endIndex():number {
        return this.startIndex + this.value.toString().length - 1
    }
}
class NotFoundGridNumber extends GridNumber {
    constructor() {
        super(-1, 0, 0)
    }
}

test.each([
    ['123', 0, new GridNumber(0,123, 0)],
    ['...123', 0, new GridNumber(3,123, 0)],
    ['....423...', 0, new GridNumber(4,423, 0)],
    ['....423..4567.', 0, new GridNumber(4,423, 0)],
    ['...956...5678...', 6, new GridNumber(9,5678, 0)],
    ['..67...', 4, new NotFoundGridNumber()],
])('given a row [%s] and a start index [%i] then return next number %o', (row: string, startIndex: number, expectedGridNumber: GridNumber) => {
    expect(nextNumberOnRow(row, startIndex, 0)).toEqual(expectedGridNumber);
})

test.each([
    [ [  '..123#.', '.......'], 0, true ], // symbol on the right
    [ [  '..123', '.....'], 0, false ], // no more char on the right
    [ [  '.#123', '.....'], 0, true ], // symbol on the left
    [ [  '123..', '.....'], 0, false], // no more char on the left available
])('given rows [%o] and a row index [%i] then return is a part number : [%s]', 
        (rows: string[], rowIndex: number, expectedIsPartNumber: boolean) => {
    expect(nextNumberOnRow(rows[rowIndex], 0, 0).isPartNumber(rows)).toBe(expectedIsPartNumber)
})

test.each([
    [ [  '..12..', '....*.'], 0, true ], // symbol below on the right corner below
    [ [  '..12..', '...*..'], 0, true ], // symbol below
    [ [  '..12..', '..*...'], 0, true ], // symbol below
    [ [  '..12..', '.*....'], 0, true ], // symbol below on the left corner
    [ [  '..12..', '......'], 0, false ], // no symbol below 
    [ [  '..12..'], 0, false ], // no row below 
])('given rows [%o] and and a row index [%i] when symbol on the row below then return is a part number : [%s]', 
        (rows: string[], rowIndex: number, expectedIsPartNumber: boolean) => {
    expect(nextNumberOnRow(rows[rowIndex], 0, 0).isPartNumber(rows)).toBe(expectedIsPartNumber)
})

test.each([
    [ [ '....*.', '..12..'], 1, true ], // symbol above on the right corner
    [ [ '...*..', '..12..'], 1, true ], // symbol above 
    [ [ '..*...', '..12..'], 1, true ], // symbol above 
    [ [ '.*....', '..12..'], 1, true ], // symbol above on the left corner
    [ [ '......', '..12..'], 1, false ], // no symbol above 
])('given rows [%o] and and a row index [%i] when symbol on the row above then return is a part number : [%s]', 
        (rows: string[], rowIndex: number, expectedIsPartNumber: boolean) => {
    expect(nextNumberOnRow(rows[rowIndex], 0, rowIndex).isPartNumber(rows)).toBe(expectedIsPartNumber)
})

test.each([
    [[  '.........', 
        '...*......'], 0, 0], // no part number
    [[  '..12..', 
        '......'], 0, 0], // not a part number
    [[  '..12..', 
        '..*...'], 0, 12],// one part number
    [[  '..12..34..', 
        '..*...*...'], 0, (12 + 34)], // two part numbers
    [[  '..12..34....87..', 
        '..*...*.........'], 0, (12 + 34)], // two part numbers a another number
    // TODO cas avec plusieurs numéros qui sont des part numbers
])('given rows [%o] with a row at index [%i] the sum is %i', (rows: string[], rowIndex: number, expectedSum: number) => {
    expect(sumOfRowPartNumbers(rows, rowIndex)).toBe(expectedSum) 
})

test('day 3 part 1 sample', () => {
    const filecontet = readFileContent('sample_day3_part1.txt')
    const input = stringWithLineSeparatorToArray(filecontet)
    expect(sumOfPartNumbers(input)).toBe(4361)
})

test('day 3 part 1', () => {
    const filecontet = readFileContent('day3.txt')
    const input = stringWithLineSeparatorToArray(filecontet)
    expect(sumOfPartNumbers(input)).toBe(533775)
})
