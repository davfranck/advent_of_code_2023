import { expect, test } from "vitest"
import { readFileContent, stringWithLineSeparatorToArray } from "./utils"

function gearsRatio(symbols: GridSymbolWithNumber[]): number {
    const symbolsMap = symbols.reduce((acc, symbol) => {
        const key = `${symbol.rowIndex}-${symbol.columnIndex}`;
        if (!acc.has(key)) {
            acc.set(key, []);
        }
        acc.get(key)?.push(symbol);
        return acc;
    }, new Map<string, GridSymbolWithNumber[]>())
    return [...symbolsMap.values()]
                .filter(gridSymbols => gridSymbols.length > 1)
                .map(gridSymbols => gridSymbols.map(gs => gs.gridNumberValue).reduce((acc, cur) => acc * cur), 1)
                .reduce((acc, cur) => acc + cur, 0)
}

class GridSymbolWithNumber {
    constructor(public rowIndex: number, public columnIndex: number, public gridNumberValue: number) {}
}

function allSymbolsForNumbers(gridNumbers: GridNumber[], rows: string[]) : GridSymbolWithNumber[] {
    return gridNumbers.flatMap(gridNumber => gridNumber.symbolsAround(rows));
}

function allGridNumbers(rows: string[]) {
    return rows.map((row: string, index: number) => gridNumbersForRow(row, index))
                .flat();
}

function cleanGrid(grid: string) {
    return grid.replace(/[^0-9.*\n]+/g, '.')
}

function gridNumbersForRow(row: string, rowIndex: number, currentIndex: number = 0): GridNumber[] {
    const partNumber = nextNumberOnRow(row, currentIndex, rowIndex);

    if (partNumber instanceof NotFoundGridNumber) {
        return [];
    } else {
        return [partNumber, ...gridNumbersForRow(row, rowIndex, partNumber.endIndex() + 1)];
    }
}

function nextNumberOnRow(row: string, startIndex: number, rowIndex: number): GridNumber {
    const match = row.slice(startIndex).match(/\d+/);
    if (match && match.index !== undefined) {
        const start = startIndex + match.index;
        return new GridNumber(start, Number(match[0]), rowIndex);
    }
    return new NotFoundGridNumber();
}

class GridNumber {
    constructor(public startIndex: number, public value: number, public rowIndex: number) {}
    
    symbolsAround(rows: string[]): GridSymbolWithNumber[] {
        const result = []
        // check à droite même ligne
        const sameRow = rows[this.rowIndex]
        if(this.endIndex() + 1 < sameRow.length && sameRow.charAt(this.endIndex() + 1) !== '.') {
            result.push(new GridSymbolWithNumber(this.rowIndex, this.endIndex() +1, this.value));
        }
        // à gauche même ligne
        if(this.startIndex !== 0 && sameRow.charAt(this.startIndex - 1) !== '.') {
            result.push(new GridSymbolWithNumber(this.rowIndex, this.startIndex - 1, this.value));
        }
        // en dessous
        if(rows.length > this.rowIndex + 1) {
            const rowBelow = rows[this.rowIndex + 1]
            const symbolOnLine = this.symbolOnLine(sameRow.length, rowBelow, this.rowIndex + 1)
            if(symbolOnLine != null) {
                result.push(symbolOnLine);
            }
        }
        // au dessus
        if(this.rowIndex - 1 >= 0) {
            const rowAbove = rows[this.rowIndex - 1]
            const symbolOnLine = this.symbolOnLine(sameRow.length, rowAbove, this.rowIndex - 1)
            if(symbolOnLine != null) {
                result.push(symbolOnLine);
            }
        }

        return result;
    }

    symbolOnLine(rowLength: number, row: string, rowIndex: number): GridSymbolWithNumber | null {
        const leftLimit = this.startIndex == 0 ? 0 : this.startIndex - 1
        const rightLimit = this.endIndex() + 1 === rowLength ? rowLength - 1: this.endIndex() + 1
        for(let i = leftLimit; i <= rightLimit; i++) {
            if(row.charAt(i) !== '.') {
                return new GridSymbolWithNumber(rowIndex, i, this.value);
            }
        }
        return null;
    }

    endIndex():number {
        return this.startIndex + this.value.toString().length - 1
    }
}
class NotFoundGridNumber extends GridNumber {
    constructor() {
        super(-1, 0, 0)
    }
}

test.each([
    ['123', 0, new GridNumber(0,123, 0)],
    ['...123', 0, new GridNumber(3,123, 0)],
    ['....423...', 0, new GridNumber(4,423, 0)],
    ['....423..4567.', 0, new GridNumber(4,423, 0)],
    ['...956...5678...', 6, new GridNumber(9,5678, 0)],
    ['..67...', 4, new NotFoundGridNumber()],
])('given a row [%s] and a start index [%i] then return next number %o', (row: string, startIndex: number, expectedGridNumber: GridNumber) => {
    expect(nextNumberOnRow(row, startIndex, 0)).toEqual(expectedGridNumber);
})

test.each([
    [`..12..*..#..\n...56+..*.4.`, 
     `..12..*.....\n...56...*.4.`],
    [`..12..*../..\n...56@..*.4.`, 
     `..12..*.....\n...56...*.4.`]
])('given a grid then should remove all symbols except *', (grid: string, expectedGrid: string) => {
    expect(cleanGrid(grid)).toEqual(expectedGrid)
})

test.each([
    ['12..45.789', [new GridNumber(0, 12, 0), new GridNumber(4, 45, 0), new GridNumber(7, 789, 0)]]
])('all grid numbers on a row', (row:string, expectedNumbers: GridNumber[]) => {
    expect(gridNumbersForRow(row, 0)).toEqual(expectedNumbers);
})

test.each([
    [
        ['..12..34..56',
         '..*...*..279'],
        [new GridNumber(2, 12, 0), new GridNumber(6, 34, 0), new GridNumber(10, 56, 0), new GridNumber(9,279, 1)]
    ]
])
('given a grid then should retrieve all GridNumbers', (rows: string[], expectedGridNumbers: GridNumber[]) => {
    expect(allGridNumbers(rows)).toEqual(expectedGridNumbers)
})

test.each([
[`..12..34..56
 ..*.......*.
 ..78......9.
 ...*........`,
 [new GridSymbolWithNumber(1, 2, 12), new GridSymbolWithNumber(1, 10, 56), new GridSymbolWithNumber(3, 3, 78), new GridSymbolWithNumber(1, 2, 78), new GridSymbolWithNumber(1, 10, 9)]]
])('given a list of grid numbers then retrieve all symbols', (grid: string, expectedSymbols: GridSymbolWithNumber[]) => {
    const rows = stringWithLineSeparatorToArray(grid)
    const gridNumbers = allGridNumbers(rows)
    expect(allSymbolsForNumbers(gridNumbers, rows)).toEqual(expectedSymbols)
})


test('gear ratio sample', () => {
    const content = readFileContent('sample_day3_part2.txt')
    const rows = stringWithLineSeparatorToArray(content)
    const gridNumbers = allGridNumbers(rows)
    const symbols = allSymbolsForNumbers(gridNumbers, rows)
    expect(gearsRatio(symbols)).toEqual(467835);
})

test('gear ratio full file', () => {
    const content = readFileContent('day3.txt')
    const rows = stringWithLineSeparatorToArray(content)
    const gridNumbers = allGridNumbers(rows)
    const symbols = allSymbolsForNumbers(gridNumbers, rows)
    expect(gearsRatio(symbols)).toEqual(78236071);
})