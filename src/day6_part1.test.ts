import { expect, test } from "vitest";
import { readFileContent, stringWithLineSeparatorToArray } from "./utils";

class Race{
    
    constructor(public time: number, public bestDistance: number) {}

    static from(lines: string[]): Race[] {
        const times = lines[0].match(/\d+/g)!.map(nb => Number.parseInt(nb))
        const bestDistances = lines[1].match(/\d+/g)!.map(nb => Number.parseInt(nb))
        const result = []
        for(let i = 0; i < times.length; i++) {
            result.push(new Race(times[i], bestDistances[i]))
        }
        return result;
    }

    betterHoldMilliseconds(): number[] {
        const result = []
        for(let i = 1; i < this.time; i++) {
            const millimeters = this.millimetersForTime(i) 
            if (millimeters > this.bestDistance) {
                result.push(i);
            }
        }
        return result
    }
    millimetersForTime(holdTime: number) {
        return (this.time - holdTime) * holdTime;
    }
}

test('given the part 1 input file then return the result', () => {
    const content = readFileContent('day6.txt');
    const lines = stringWithLineSeparatorToArray(content)
    const result = Race.from(lines)
                        .map(race => race.betterHoldMilliseconds().length)
                        .reduce((acc, cur) => (acc * cur), 1)
    expect(result).toBe(303600)
})

test('given the part 1 sample input file then return the result', () => {
    const content = readFileContent('sample_day6_part1.txt')
    const lines = stringWithLineSeparatorToArray(content)
    const result = Race.from(lines)
                        .map(race => race.betterHoldMilliseconds().length)
                        .reduce((acc, cur) => (acc * cur), 1)
    expect(result).toBe(288)
})

test('given the sample input file then return races', () => {
    const content = readFileContent('sample_day6_part1.txt')
    const lines = stringWithLineSeparatorToArray(content)
    expect(Race.from(lines)).toEqual([new Race(7,9), new Race(15,40), new Race(30,200)])
})

test. each([
    [new Race(7, 19), []],
    [new Race(7, 9), [2,3,4,5]],
    [new Race(15, 40), [4,5,6,7,8,9,10,11]],
    [new Race(30, 200), [11,12,13,14,15,16,17,18,19]],
])('given a race %o when betterHoldMilliseconds then return the milliseconds %o that improve the record', 
        (race:Race, expectedBetterHoldMilliseconds: number[]) => {
    expect(race.betterHoldMilliseconds()).toEqual(expectedBetterHoldMilliseconds);
})

test.each([
    [1, 7, 6],
    [1, 6, 5],
    [2, 7, 10],
    [3, 7, 12],
    [6, 7, 6],
])('given a hold time %i and a race time %i then return distance %i', (holdTime: number, raceTime: number, distance: number) =>{
    expect(new Race(raceTime, 0).millimetersForTime(holdTime)).toBe(distance)
})