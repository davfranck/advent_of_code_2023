# Journal de l'advent of code 2023
Cette année ce sera Typescript mais surtout avec ChatGPT pour faire du pair programming. 

## Docs
* https://vitest.dev/api/

## Day 1
La partie 1 se passe plutôt pas mal. 
Je dois affiner mon prompt pour qu'il ne me donne pas plus que ce qui est nécessaire. 
Au départ ça donne donc : 
```
You are a typescript expert.  We will do pair programming and TDD. We are using vitest. 
I will give you a test and you will give me the implementation. Don't code anything that is not needed by the test. 
Are your ready ?
```

Pour la partie 2, impossible dans un premier temps de faire passer le nouveau besoin (transformation de chiffres au format texte vers des chiffres au format arabe). 
En descendant sur une fonction dédiée et en résolvant ce besoin uniquement c'est mieux : le cas d'exemple passe. 
En revanche le cas cible ne passe pas. 
Il doit y avoir un (ou des) cas limites que je n'ai pas vu. 

Trouvé : pcg91vqrfpxxzzzoneightzt : le eight de la fin je ne le vois pas, je vois le 1 ! 
=> nouvelle une implémentation en cherchant à remplacer le premier en partant de la gauche, puis le premier en partant de la fin. 
C'est pas si simple du tout d'écrire le bon cas de test. 
Et c'est très intéressant : en effet il faut vraiment donner le bon exemple. 
Ca force aussi a écrire un bon nom de test, bien complet avec le given/when/then :

Je me rends compte là encore que ce n'est pas si simple, donc je descends d'un niveau de test / méthode. 
J'ajoute un findFirst et un findLast.  

Bon finalement grosse prise de tête pour faire passer le findLast. 
Du coup je l'ai fait à la mano.

## Day 2
Partie 1 : j'ai essayé d'avancer entre 2 autres activités et du coup c'était du de suivre le fil. 
J'ai laissé passer un espace à un moment et je n'avais pas le bon test pour m'en rendre compte. 
J'aurais du descendre d'une vitesse dans mes steps. 
Et surtout prendre le temps de faire les refactos. 

Au final je trouve le code pas très jojo. 

## Day 3
Je vois que je n'ai pas introduit un concept de Row et je crois que ça aurait clarifier le code. 
A voir si j'ai le courage de le faire. 
Sinon autre fois (#TODO). 

En refacto avec GPT j'apprends : 
* que je dois utiliser les valeurs par défaut dans les paramètres de méthodes, ça m'aurait permis de perdre moins de temps sur certains refactos pendant le kata
* il y a des paramètres autres que la valeur qui sont disponibles dans les méthodes d'itération sur un tableau (par exemple on a l'index sur un map)
* reduce ça marche bien pour faire une construction de map 
* il m'a proposé une réécriture très intéressante sur une utilisation de regex

## Day 4
Aujourd'hui, utilisation de `match` pour extraire les nombre, c'est efficace ! 
Usage aussi de `includes` sur un tableau pour tester s'il contient un nombre. 